---
layout: post
title:  "Push Notififcation Subscriber WordPress"
date:   2016-08-18 10:20:03 +0530
categories: wordpress
tags: 
    - wordpress
    - rest
    - notifications
author: rsakhale
---
Often there are several notification mechanism, within which wordpress users from a device can subscribe to wordpress for enhanced notification experience.
<!--more-->
### Push Notifications

1. Google Cloud Messaging
1. Apple Push Notification
1. Mozilla Push Notification
1. Ubuntu Push Notification
1. Windows Push Notification

### Use cases

1. Update individual item data for offline application
1. Notify users for new post
