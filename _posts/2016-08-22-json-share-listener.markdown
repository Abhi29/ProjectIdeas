---
layout: post
title:  "JSON Share Listener - JMeter Plugin"
date:   2016-08-22 08:35:03 +0530
categories: jmeter plugins
tags: 
    - automation
    - jmeter
    - plugins
    - json
    - share
author: rsakhale
---

Often there comes an ability of sharing some values from one request and use them into another request to form scenario based testing.
<!--more-->