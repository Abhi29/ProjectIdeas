---
layout: post
title:  "REST Framework with JMeter"
date:   2016-08-22 08:35:03 +0530
categories: jmeter plugins
tags: 
    - automation
    - jmeter
    - framework
    - rest
author: rsakhale
---

JMeter is a performance testing tool wherein we can perform several types of request.
<!--more-->
REST is among the easiest form of HTTP Request that we can perform with the help of multiple verbs available as HTTP Methods.